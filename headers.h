#ifndef _EVENT_H_
#define _EVENT_H_
#include <vector>

class Event {
private:
  std::string name;
  std::string eventType;
  std::string date;
  std::string eventFormat;
public:
  int eventId;
  void setName (std::string name);
  void setDate (std::string date);
  void setEventType (std::string eventType);
  std::string getName ();
  std::string getEventType ();
  std::string getDate ();
};
class Film:public Event {
public:
  std::string eventFormat;
  std::vector < int >reservedSeatedSeats;
};
class Music: public Event {
public:
  std::vector < int >reservedSeatedSeats;
};
class StandUp:public Event {
public:
  std::vector < int >reservedSeatedSeats;
};
void addBooking (int searchedId,
		 int seatNr,
		 std::vector < Film > &filmVector,
		 std::vector < Music > &musicVector,
		 std::vector < StandUp > &standupVector);
void cancelBooking (int searchedId,
		    int seatNr,
		    std::vector < Film > & filmVector,
		    std::vector < Music > & musicVector,
		    std::vector < StandUp > & standupVector);
void loadFromFile (std::vector < Film > &filmObject,
		   std::vector < Music > &musicObject,
		   std::vector < StandUp > &standupObject);
void saveToFile (std::vector < Film > &filmVector,
		 std::vector < Music > &musicVector,
		 std::vector < StandUp > &standupVector);
void listAllEvents ();
void listSingleEvent (int searchedId);
#endif
