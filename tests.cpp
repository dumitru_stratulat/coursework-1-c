
#include "catch.hpp"
#include "headers.h"

TEST_CASE("Test film class methods","[film]"){
  Film film;
  std::vector<Film> filmVector;
  std::vector<Music> mock1;
  std::vector<StandUp> mock2;
  film.eventId = 32132;
  film.setName("Friends");
  film.setName("Friends");
  film.setEventType("film");
  film.setDate("20.01.2022");
  film.reservedSeatedSeats.push_back(100);
  film.reservedSeatedSeats.push_back(101);
  REQUIRE(film.getName() == "Friends");
  REQUIRE(film.getEventType() == "film");
  REQUIRE(film.getDate() == "20.01.2022");
  REQUIRE(film.reservedSeatedSeats[0] == 100);
  REQUIRE(film.reservedSeatedSeats.size() == 2);
  REQUIRE(film.eventId == 32132);
  filmVector.push_back(film);
  addBooking(32132,777,filmVector,mock1,mock2);
  REQUIRE(filmVector[0].reservedSeatedSeats.size() == 3);
}
TEST_CASE("Test music class methods","[music]"){
  Music music;
  
  music.setName("Celentano");
  music.setEventType("music");
  music.setDate("20.01.2022");
  music.reservedSeatedSeats.push_back(100);
  music.reservedSeatedSeats.push_back(101);
  REQUIRE(music.getName() == "Celentano");
  REQUIRE(music.getEventType() == "music");
  REQUIRE(music.getDate() == "20.01.2022");
  REQUIRE(music.reservedSeatedSeats[0] == 100);
  REQUIRE(music.reservedSeatedSeats.size() == 2);
}
TEST_CASE("Test standup class methods","[standup]"){
  StandUp standup;
  
  standup.setName("Friends");
  standup.setEventType("standup");
  standup.setDate("20.01.2022");
  standup.reservedSeatedSeats.push_back(100);
  standup.reservedSeatedSeats.push_back(101);
  REQUIRE(standup.getName() == "Friends");
  REQUIRE(standup.getEventType() == "standup");
  REQUIRE(standup.getDate() == "20.01.2022");
  REQUIRE(standup.reservedSeatedSeats[0] == 100);
  REQUIRE(standup.reservedSeatedSeats.size() == 2);
}

TEST_CASE("Test addBooking and cancelBooking","[addBooking]"){
  Film film;
  Music music;
  StandUp standup;
  
  std::vector<Film> filmVector;
  std::vector<Music> musicVector;
  std::vector<StandUp> standupVector;
  
  film.eventId = 777;
  film.setName("Friends");
  film.setEventType("film");
  film.setDate("20.01.2022");
  filmVector.push_back(film);

  music.eventId = 778;
  music.setName("Celentano");
  music.setEventType("music");
  music.setDate("21:30.20.01.2022");
  musicVector.push_back(music);

  standup.eventId = 779;
  standup.setName("Comedy");
  standup.setEventType("standup");
  standup.setDate("22::00.20.01.2022");
  standupVector.push_back(standup);
  
  addBooking(777,100,filmVector,musicVector,standupVector);
  addBooking(778,100,filmVector,musicVector,standupVector);
  addBooking(779,100,filmVector,musicVector,standupVector);

  REQUIRE(filmVector[0].reservedSeatedSeats[0] == 100);
  REQUIRE(musicVector[0].reservedSeatedSeats[0] == 100);
  REQUIRE(standupVector[0].reservedSeatedSeats[0] == 100);

  }

