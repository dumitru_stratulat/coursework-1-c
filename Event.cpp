#define CATCH_CONFIG_RUNNER
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdbool.h>
#include "headers.h"
#include "catch.hpp"

std::string Event::getName() {
  return this -> name;
}
void Event::setName(std::string name) {
  this -> name = name;
}
void Event::setEventType(std::string eventType) {
  this -> eventType = eventType;
}
std::string Event::getEventType() {
  return this -> eventType;
}
void Event::setDate(std::string date) {
  this -> date = date;
}
std::string Event::getDate() {
  return this -> date;
}
int main(int argc, char * argv[]) {
  bool menuState = true;
  int searchId;
  int searchSeatNr;

  std::vector < Film > filmObject;
  std::vector < Music > musicObject;
  std::vector < StandUp > standupObject;

  while (menuState) {
    int userInput;
    std::cout << "===Menu===" << std::endl;
    std::cout << "1.Load data from file" << std::endl;
    std::cout << "2.Add booking" << std::endl;
    std::cout << "3.Cancel booking" << std::endl;
    std::cout << "4.List all events" << std::endl;
    std::cout << "5.List specific event" << std::endl;
    std::cout << "6.Save data to file" << std::endl;
    std::cout << "7.Exit" << std::endl;
    //throw error if entered string instead of int
    while (!(std::cin >> userInput)) {
      std::cout << "You entered a non-digit character, nr of the command \n";
      std::cin.clear();
      std::cin.ignore();
    }
    if(userInput > 7 || userInput < 1) {
      std::cout<<"Enter the range from 1 to 7!"<<std::endl;
    }    
    if (userInput == 1) {
      loadFromFile(filmObject, musicObject, standupObject);
      std::cout << "Finished to load data from file" << std::endl;
    }
    if (userInput == 2) {
      std::cout << "Insert event id!" << std::endl;
      std::cin >> searchId;
      std::cout << "Insert seat!" << std::endl;
      std::cin >> searchSeatNr;
      loadFromFile(filmObject, musicObject, standupObject);
      addBooking(searchId, searchSeatNr, filmObject, musicObject, standupObject);
    }
    if (userInput == 3) {
      std::cout<<"Introduce eventId"<<std::endl;
      std::cin>>searchId;
      std::cout<<"Introduce nr of the seat"<<std::endl;
      std::cin>>searchSeatNr;
      cancelBooking(searchId,searchSeatNr,filmObject, musicObject, standupObject);
    }
    if (userInput == 4) {
      loadFromFile(filmObject, musicObject, standupObject);
      listAllEvents();
    }
    if (userInput == 5) {
      std::cout << "Introduce id of specific event!" << std::endl;
      while (!(std::cin >> searchId)) {
        std::cout << "You entered a non-digit character, introduce id of event \n";
        std::cin.clear(); 
        std::cin.ignore();
      }
      listSingleEvent(searchId);
    }
    if (userInput == 6) {
      saveToFile(filmObject, musicObject, standupObject);
      std::cout<<"Data saved!"<<std::endl;
    }
    if (userInput == 7) {
      menuState = false;
    }
  }
  int result = Catch::Session().run(argc, argv);
  
  return result;
};
