CXX = g++
CXXFLAGS = -g -Wall -Wextra -Wpedantic -std=c++11 
output: Event.o
	$(CXX) $(CXXFLAGS)-c -o output Event.o  
Event.o: Event.cpp headers.h menuFunctionalityMethods.cpp
	$(CXX) $(CXXFLAGS) Event.cpp menuFunctionalityMethods.cpp -o Event.o
tests.o: tests.cpp
	$(CXX) $(CXXFLAGS) tests.cpp Event.cpp  menuFunctionalityMethods.cpp -o test.exe
clean:
	rm *.o output *.exe
