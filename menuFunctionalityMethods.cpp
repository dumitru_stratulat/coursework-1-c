#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "headers.h"

//checking every vector of objects if id match searched id
void addBooking(int searchedId,
		int seatNr,
		std::vector < Film > & filmVector,
		std::vector < Music > & musicVector,
		std::vector < StandUp > & standupVector) {
  bool idFound = false;
  for (size_t i = 0; i < filmVector.size(); i++) {
    if (filmVector.size() >= 200){
      std::cout<<"This event is fully booked!"std::endl;
      break;
    }
    if (musicVector.size() >= 300){
      std::cout<<"This event is fully booked!"<<std::end;
      break;
    }
    if (standupVector.size() >= 200){
      std::cout<<"This event is fully booked!"<<std::endl;
      break;
    }
    if (filmVector[i].eventId == searchedId) {
      idFound = true;
      filmVector[i].reservedSeatedSeats.push_back(seatNr);
      break;
    }
  }
  if (idFound != true) {
    for (size_t i = 0; i < musicVector.size(); i++) {
      if (musicVector[i].eventId == searchedId) {
        idFound = true;
        musicVector[i].reservedSeatedSeats.push_back(seatNr);
        break;
      }
    }
  }
  if (idFound != true) {
    for (size_t i = 0; i < standupVector.size(); i++) {
      if (standupVector[i].eventId == searchedId) {
        idFound = true;
        standupVector[i].reservedSeatedSeats.push_back(seatNr);
        break;
      }
    }
  }
}
void cancelBooking(int searchedId,
		   int seatNr,
		   std::vector < Film > & filmVector,
		   std::vector < Music > & musicVector,
		   std::vector < StandUp > & standupVector) {
  bool idFound = false;
  for (size_t i = 0; i < filmVector.size(); i++) {
    if (filmVector[i].eventId == searchedId) {
      idFound = true;
      filmVector[i].reservedSeatedSeats.erase(std::remove(filmVector[i].reservedSeatedSeats.begin(),
							  filmVector[i].reservedSeatedSeats.end(),
							  seatNr),
					                  filmVector[i].reservedSeatedSeats.end());
      std::cout<<"Booking has been canceled!"<<std::endl;
      break;
    }
  }
  if (idFound != true) {
    for (size_t i = 0; i < musicVector.size(); i++) {
      if (musicVector[i].eventId == searchedId) {
        idFound = true;
        musicVector[i].reservedSeatedSeats.erase(std::remove(musicVector[i].reservedSeatedSeats.begin(),
							     musicVector[i].reservedSeatedSeats.end(),
							     seatNr),
					                     musicVector[i].reservedSeatedSeats.end());
        std::cout<<"Booking has been canceled!"<<std::endl;
        break;
      }
    }
  }
  if (idFound != true) {
    for (size_t i = 0; i < standupVector.size(); i++) {
      if (standupVector[i].eventId == searchedId) {
        idFound = true;
        standupVector[i].reservedSeatedSeats.erase(std::remove(standupVector[i].reservedSeatedSeats.begin(),
							      standupVector[i].reservedSeatedSeats.end(),
							      seatNr),
					                      standupVector[i].reservedSeatedSeats.end());
        std::cout<<"Booking has been canceled!"<<std::endl;
        break;
      }
    }
  }
}
//loading data from file by parsing every line every word and inserting in objects
void loadFromFile(
		  std::vector < Film > & filmObject,
		  std::vector < Music > & musicObject,
		  std::vector < StandUp > & standupObject) {
  filmObject.clear();
  musicObject.clear();
  standupObject.clear();
  Film film;
  Music music;
  StandUp standup;
  //defining local variables
  int id;
  std::string name;
  std::string eventType;
  std::string date;
  std::string line;
  int nr;

  std::ifstream in_file;
  in_file.open("data.txt");
  if (in_file.fail()) {
    std::cout << "Instream file failed";
  }
  while (std::getline(in_file, line)) { //getting every line
    std::vector < int > tempVector; //temporary vector
    int indexOfLastLetterInWord;
    //getting id and erasing from the line
    indexOfLastLetterInWord = line.find(" ");
    id = std::stoi(line.substr(0, indexOfLastLetterInWord)); //stoi function to convert 
    line.erase(0, indexOfLastLetterInWord + 1); //erase the id
    //getting name and erasing from the line
    indexOfLastLetterInWord = line.find(" ");
    name = line.substr(0, indexOfLastLetterInWord);
    line.erase(0, indexOfLastLetterInWord + 1);
    //getting eventType and erasing from the line
    indexOfLastLetterInWord = line.find(" ");
    eventType = line.substr(0, indexOfLastLetterInWord);
    line.erase(0, indexOfLastLetterInWord + 1);
    //getting date and erasing from the line
    indexOfLastLetterInWord = line.find(" ");
    date = line.substr(0, indexOfLastLetterInWord);
    //check if line contain space character
    if (indexOfLastLetterInWord + 1 == 0) {
      line.erase(0, indexOfLastLetterInWord);
    } else {
      line.erase(0, indexOfLastLetterInWord + 1);
    };
    //getting seat numbers and pushing to vector
    while (line.length() != 0) {
      if (line.length() > 0) {
        indexOfLastLetterInWord = line.find(" ");
        nr = std::stoi(line.substr(0, indexOfLastLetterInWord));
        tempVector.push_back(nr);

        if (indexOfLastLetterInWord + 1 == 0) {
          line.erase(0, indexOfLastLetterInWord);
        } else {
          line.erase(0, indexOfLastLetterInWord + 1);
        };
      }
    }
    //pushing data in object 
    if (eventType == "film") {
      film.eventId = id;
      film.setName(name);
      film.setEventType(eventType);
      film.setDate(date);
      film.reservedSeatedSeats = tempVector;
      filmObject.push_back(film);
    }
    if (eventType == "music") {
      music.eventId = id;
      music.setName(name);
      music.setEventType(eventType);
      music.setDate(date);
      music.reservedSeatedSeats = tempVector;
      musicObject.push_back(music);
    }
    if (eventType == "standup") {
      standup.eventId = id;
      standup.setName(name);
      standup.setEventType(eventType);
      standup.setDate(date);
      standup.reservedSeatedSeats = tempVector;
      standupObject.push_back(standup);
    }
  }
  in_file.close();
}
void saveToFile(
		std::vector < Film > & filmVector,
		std::vector < Music > & musicVector,
		std::vector < StandUp > & standupVector) {
  std::ofstream of_file;
  of_file.open("data.txt"); //open file for writing
  //saving to file all data from objects
  for (Film film: filmVector) {
    of_file << film.eventId << " ";
    of_file << film.getName() << " ";
    of_file << film.getEventType() << " ";
    of_file << film.getDate();
    for (int seat: film.reservedSeatedSeats) {
      of_file << " ";
      of_file << seat;
    }
    of_file << "\n";
  }
  for (Music music: musicVector) {
    of_file << music.eventId << " ";
    of_file << music.getName() << " ";
    of_file << music.getEventType() << " ";
    of_file << music.getDate();
    for (int seat: music.reservedSeatedSeats) {
      of_file << " ";
      of_file << seat;
    }
    of_file << "\n";
  }
  for (StandUp standup: standupVector) {
    of_file << standup.eventId << " ";
    of_file << standup.getName() << " ";
    of_file << standup.getEventType() << " ";
    of_file << standup.getDate();
    for (int seat: standup.reservedSeatedSeats) {
      of_file << " ";
      of_file << seat;
    }
    of_file << "\n";
  }
  of_file.close();
}
//getting all events line by line in file
void listAllEvents() {
  std::string line;
  std::ifstream in_file;
  in_file.open("data.txt");
  if (in_file.fail()) {
    std::cout << "Instream file failed";
  }
  for (int i = 0; i < 5; i++) {
    std::cout << std::endl;
  }
  std::cout << "===All events===" << std::endl;
  while (std::getline(in_file, line)) {
    std::cout << line << std::endl;
  }
  for (int i = 0; i < 5; i++) {
    std::cout << std::endl;
  }
  in_file.close();
}
//getting event by parsing every line id
void listSingleEvent(int searchedId) {
  int id;
  std::string line;
  std::ifstream in_file;
  in_file.open("data.txt");
  if (in_file.fail()) {
    std::cout << "Instream file failed";
  }
  while (std::getline(in_file, line)) {
    int indexOfLastLetterInWord;
    indexOfLastLetterInWord = line.find(" ");
    id = std::stoi(line.substr(0, indexOfLastLetterInWord));
    if (searchedId == id) {
      for (int i = 0; i < 5; i++) {
        std::cout << std::endl;
      }
      std::cout << line << std::endl;
      for (int i = 0; i < 5; i++) {
        std::cout << std::endl;
      }
    }
  }
  in_file.close();
}
